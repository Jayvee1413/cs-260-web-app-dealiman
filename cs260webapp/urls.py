"""cs260webapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from dealiman.views import *

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', list_advertisements),
    url(r'^login/', 'django.contrib.auth.views.login'),
    url(r'^logout/$', logout_page),
    url(r'^register/$', register),
    url(r'^register/success/$', register_success),
    url(r'^advertisement/add/$', add_advertisement),
    url(r'^advertisement/view/(?P<ad_id>\d{1,8})/$', view_advertisement),
    url(r'^advertisement/view/$', list_advertisements),
    url(r'^administration/for_approval/$', admin_list_ads_for_approval),
    url(r'^administration/for_approval/show/[0-9]{1,8}$', admin_show_ad_for_approval),
    url(r'^administration/approve/$', admin_approve_ad),
]
