from django.db import models

# Create your models here.

from django.contrib.auth.models import User

class AppUser(models.Model):
    user = models.OneToOneField(User)
    mobile_number = models.CharField(max_length=100)

class Advertisement(models.Model):
    CATEGORY_CHOICES = (
        ('BOOKS', 'Books'),
        ('MOBILE', 'Mobile'),
        ('COMPUTERS', 'Computers'),
        ('CLOTHES', 'Clothes'),
        ('APPLIANCES', 'Appliances'),
        ('FURNITURE', 'Furniture'),
        ('SERVICES', 'Services'),
    )

    STATUS_CHOICES = (
        ('FOR APPROVAL', 'For Approval'),
        ('APPROVED', 'Approved'),
        ('REJECTED', 'Rejected'),
        ('EXPIRED', 'Expired'),
        ('ARCHIVED', 'Archived')
    )

    user = models.ForeignKey(User)
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=1024)
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=50)
    price = models.DecimalField(decimal_places=2, max_digits=10)
    price_negotiable = models.BooleanField()
    is_used = models.BooleanField()
    quantity = models.IntegerField()
    pic1 = models.TextField(max_length=1024)
    pic2 = models.TextField(max_length=1024)
    pic3 = models.TextField(max_length=1024)
    pic4 = models.TextField(max_length=1024)
    pic5 = models.TextField(max_length=1024)
    posting_date = models.DateTimeField(auto_now_add = True)
    status = models.CharField(choices=STATUS_CHOICES, max_length=50, default='FOR APPROVAL')