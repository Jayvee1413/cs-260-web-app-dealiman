#views.py
import datetime
import os
import time

from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect

from dealiman.forms import *
from dealiman.models import AppUser, Advertisement

@csrf_protect
def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password1'],
            email=form.cleaned_data['email']
            )
            app_user = AppUser(user=user, mobile_number=form.cleaned_data['mobile_number'])
            app_user.save()
            g = Group.objects.get(name='user')
            g.user_set.add(user)
            return HttpResponseRedirect('/register/success/')
    else:
        form = RegistrationForm()
    variables = RequestContext(request, {
    'form': form
    })

    return render_to_response(
    'registration/register.html',
    variables,
    )

def register_success(request):
    return render_to_response(
    'registration/success.html',
    )

def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/')

@login_required(login_url='/login')
def add_advertisement(request):

    if request.method == 'POST':
        form = AddAdvertisementForm(request.POST, request.FILES)

        if form.is_valid():
            #get all data
            form_data = {}
            form_data['name'] = form.cleaned_data['new_advertisement_name']
            form_data['description'] = form.cleaned_data['new_advertisement_description']
            form_data['category'] = form.cleaned_data['new_advertisement_category']
            form_data['price'] = form.cleaned_data['new_advertisement_price']
            form_data['price_negotiable'] = form.cleaned_data['new_advertisement_price_negotiable']
            form_data['is_used'] = form.cleaned_data['new_advertisement_is_used']
            form_data['quantity'] = form.cleaned_data['new_advertisement_quantity']

            files = {}

            for pic in range(1,6):
                pic_str = 'pic_%s' % pic

                if 'new_advertisement_%s' % pic_str in request.FILES:
                    timestr = time.strftime("%Y%m%d-%H%M%S")
                    file =  request.FILES['new_advertisement_%s' % pic_str]
                    extension = file.content_type.split('/')[1]
                    file_name = "%s_%s.%s" % (timestr, pic, extension)
                    form_data['%s' % pic_str] = file_name
                    files['%s' % pic_str] = {'file_name': file_name, 'file': file}
                else:
                    form_data['%s' % pic_str] = ''

            # save to model

            new_advertisement = Advertisement(
                user=request.user,
                name=form_data['name'],
                description=form_data['description'],
                category=form_data['category'],
                price=form_data['price'],
                price_negotiable=form_data['price_negotiable'],
                is_used=form_data['is_used'],
                quantity=form_data['quantity'],
                pic1=form_data['pic_1'],
                pic2=form_data['pic_2'],
                pic3=form_data['pic_3'],
                pic4=form_data['pic_4'],
                pic5=form_data['pic_5'],
                posting_date=datetime.datetime.now()
            )
            new_advertisement.save()

            #TODO save pictures
            #create directory first
            try:
                os.makedirs("%s/%s/%s"%(os.getcwd(), 'dealiman/static/images', new_advertisement.id))
            except Exception as e:
                print(e)

            # Save pictures
            for file_keys in files.keys():
                file = files[file_keys]
                file_name = file['file_name']
                file_obj = file['file']
                with open("%s/%s/%s/%s"%(os.getcwd(), 'dealiman/static/images', new_advertisement.id, file_name),  'wb+') as f:
                    for chunk in file_obj.chunks():
                        f.write(chunk)

            app_user = AppUser(user_id=request.user.id)
            photos = []
            for count in range(1,6):
                if getattr(new_advertisement, 'pic%s'%count, ''):
                    photos.append(getattr(new_advertisement, 'pic%s'%count))

            ad_data = {
                "id" : new_advertisement.id,
                "user" : new_advertisement.user.username,
                "contact" : app_user.mobile_number,
                "name" : new_advertisement.name,
                "description" : new_advertisement.description,
                "category" : new_advertisement.category.capitalize(),
                "price" : "{:,.2f}".format(new_advertisement.price),
                "price_negotiable" : new_advertisement.price_negotiable,
                "quantity" : new_advertisement.quantity,
                "is_used" : new_advertisement.is_used,
                "posting_date" : new_advertisement.posting_date,
                "photos" : photos,
            }
            return render(request, 'advertisement/add_advertisement_success.html', {'ad': ad_data, 'photos' : photos})
    else:
        form = AddAdvertisementForm()

    return render(request, 'advertisement/add_advertisement.html', {'form': form})

def list_advertisements(request):

    ad_test = Advertisement.objects.filter(status='APPROVED')

    ad_data = list()
    
    for ad in ad_test:
        ad_content = {
            "item_id" : ad.id,
	        "item_name" : ad.name,
            "item_description" : ad.description,
            "item_category" : ad.category,
            "item_price" : ad.price,
            "item_price_negotiable" : ad.price_negotiable,
            "item_quantity" : ad.quantity,
            "item_is_used" : ad.is_used,
            "item_pic1" : ad.pic1,
            "posting_date" : ad.posting_date,
        }
    
        ad_data.append(ad_content)
    
    return render(request, 'advertisement/list.html', {'ad': ad_data})

def admin_list_ads_for_approval(request):
    for_approval = list(Advertisement.objects.filter(status='FOR APPROVAL').values('id', 'name'))
    return render(request, 'administration/list_for_approval.html', {'for_approval': for_approval})

def admin_show_ad_for_approval(request):
    path = request.path
    ad_id = path.split('/')[4]
    
    if (ad_id.isdigit()):
        ad_data = (Advertisement.objects.get(id=ad_id))

    if (ad_data):
        temp_photos = [ad_data.pic1,ad_data.pic2,ad_data.pic3,ad_data.pic4,ad_data.pic5]
        photos = [x for x in temp_photos if x is not '']
        contact = AppUser.objects.get(user=ad_data.user).mobile_number

    return render(request, 'administration/show_for_approval.html', {'ad': ad_data, 'photos': photos, 'contact':contact })

def admin_approve_ad(request):
    # TODO check if user is admin
    if request.method == 'POST':
        if request.POST['action'] == 'Approve':
            for_approval = Advertisement.objects.get(id=int(request.POST['ad_id']))
            for_approval.status = "APPROVED";
            for_approval.save()
        elif request.POST['action'] == 'Reject':
            for_approval = Advertisement.objects.get(id=int(request.POST['ad_id']))
            for_approval.status = "REJECTED";
            for_approval.save()

   ## else:
	   ##do something else
    
    success_approve = 1
    for_approval = list(Advertisement.objects.filter(status='FOR APPROVAL').values('id', 'name'))
    return render(request, 'administration/list_for_approval.html', {'for_approval': for_approval})

def view_advertisement(request, ad_id): 
    try:
        new_advertisement = Advertisement.objects.get(id=ad_id)
        app_user = AppUser.objects.get(user_id=new_advertisement.user_id)

        photos = []
        for count in range(1,6):
            if getattr(new_advertisement, 'pic%s'%count, ''):
                photos.append(getattr(new_advertisement, 'pic%s'%count))

        ad_data = {
            "id" : new_advertisement.id,
            "user" : new_advertisement.user.username,
            "contact" : app_user.mobile_number,
            "name" : new_advertisement.name,
            "description" : new_advertisement.description,
            "category" : new_advertisement.category.capitalize(),
            "price" : "{:,.2f}".format(new_advertisement.price),
            "price_negotiable" : new_advertisement.price_negotiable,
            "quantity" : new_advertisement.quantity,
            "is_used" : new_advertisement.is_used,
            "posting_date" : new_advertisement.posting_date,
            "photos" : photos
        }
    except Advertisement.DoesNotExist:
        raise Http404("Page does not exist")

    return render(request, 'advertisement/view_advertisement.html', {'ad': ad_data})
    
