#files.py
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

class RegistrationForm(forms.Form):

    username = forms.RegexField(regex=r'^\w+$', widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label=_("Username"), error_messages={ 'invalid': _("This value must contain only letters, numbers and underscores.") })
    email = forms.EmailField(widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label=_("Email address"))
    mobile_number = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=13)), label=_("Mobile Number"))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, render_value=False)), label=_("Password"))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, render_value=False)), label=_("Password (again)"))

    def clean_username(self):
        try:
            user = User.objects.get(username__iexact=self.cleaned_data['username'])
        except User.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_("The username already exists. Please try another one."))

    def clean_email(self):
        try:
            email = User.objects.get(username__iexact=self.cleaned_data['email'])
        except User.DoesNotExist:
            return self.cleaned_data['email']
        raise forms.ValidationError(_("The email already exists. Please try another one."))

    def clean(self):
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_("The two password fields did not match."))
        return self.cleaned_data

class AddAdvertisementForm(forms.Form):

    CATEGORY_CHOICES = (
        ('BOOKS', 'Books'),
        ('MOBILE', 'Mobile'),
        ('COMPUTERS', 'Computers'),
        ('CLOTHES', 'Clothes'),
        ('APPLIANCES', 'Appliances'),
        ('FURNITURE', 'Furniture'),
        ('SERVICES', 'Services'),
    )

    new_advertisement_name = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=50,
                                                                               placeholder="Advertisement Name")),
                                             label=_("Advertisement Name"))
    new_advertisement_description = forms.CharField(widget=forms.Textarea(attrs=dict(required=True, max_length=1024,
                                                                               placeholder="Description")),
                                             label=_("Description"))
    new_advertisement_category = forms.ChoiceField( widget=forms.Select(attrs=dict(required=True)), choices=CATEGORY_CHOICES, label=_("Category"))
    new_advertisement_price = forms.DecimalField(max_digits=10, decimal_places=2, label=_("Price"), widget=forms.NumberInput(attrs=dict(placeholder="Price", required=True)))
    new_advertisement_price_negotiable = forms.BooleanField(widget=forms.CheckboxInput(attrs=dict(required=False)), label=("Is Negotiable"), required=False)
    new_advertisement_is_used = forms.BooleanField(widget=forms.CheckboxInput(attrs=dict(required=False)), label=("Is Item Used"), required=False)
    new_advertisement_quantity = forms.IntegerField(widget=forms.NumberInput(attrs=dict(placeholder='Quantity')), max_value=999, min_value=0, label=_("Quantity"))
    new_advertisement_pic_1 = forms.FileField(widget=forms.ClearableFileInput(attrs=dict(required=False)), allow_empty_file=True, label=_("Photo 1"), required=False)
    new_advertisement_pic_2 = forms.FileField(widget=forms.ClearableFileInput(attrs=dict(required=False)), allow_empty_file=True, label=_("Photo 2"), required=False)
    new_advertisement_pic_3 = forms.FileField(widget=forms.ClearableFileInput(attrs=dict(required=False)), allow_empty_file=True, label=_("Photo 3"), required=False)
    new_advertisement_pic_4 = forms.FileField(widget=forms.ClearableFileInput(attrs=dict(required=False)), allow_empty_file=True, label=_("Photo 4"), required=False)
    new_advertisement_pic_5 = forms.FileField(widget=forms.ClearableFileInput(attrs=dict(required=False)), allow_empty_file=True, label=_("Photo 5"), required=False)

    def clean(self):
        if not self.cleaned_data['new_advertisement_price']:
            self.cleaned_data['new_advertisement_price'] = '0.00'
        if not self.cleaned_data['new_advertisement_quantity']:
            self.cleaned_data['new_advertisement_quantity'] = '0'
        return self.cleaned_data
