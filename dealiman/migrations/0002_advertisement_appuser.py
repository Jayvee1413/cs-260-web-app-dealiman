# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dealiman', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Advertisement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(max_length=1024)),
                ('category', models.CharField(choices=[('BOOKS', 'Books'), ('MOBILE', 'Mobile'), ('COMPUTERS', 'Computers'), ('CLOTHES', 'Clothes'), ('APPLIANCES', 'Appliances'), ('FURNITURE', 'Furniture'), ('SERVICES', 'Services')], max_length=50)),
                ('price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('price_negotiable', models.BooleanField()),
                ('is_used', models.BooleanField()),
                ('quantity', models.IntegerField()),
                ('pic1', models.TextField(max_length=1024)),
                ('pic2', models.TextField(max_length=1024)),
                ('pic3', models.TextField(max_length=1024)),
                ('pic4', models.TextField(max_length=1024)),
                ('pic5', models.TextField(max_length=1024)),
                ('posting_date', models.DateTimeField(auto_now_add=True)),
                ('status', models.CharField(max_length=50, default='FOR APPROVAL', choices=[('FOR APPROVAL', 'For Approval'), ('APPROVED', 'Approved'), ('REJECTED', 'Rejected'), ('EXPIRED', 'Expired'), ('ARCHIVED', 'Archived')])),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='AppUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('mobile_number', models.CharField(max_length=100)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
