# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

def create_groups(apps, schema_editor):

    from django.contrib.auth.models import Group
    group_types = ['user', 'admin']
    for type in group_types:
        g = Group()
        g.name = type
        g.save()

class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
         migrations.RunPython(create_groups)
    ]
