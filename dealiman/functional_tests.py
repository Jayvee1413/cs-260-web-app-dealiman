from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

"""
class RegisterTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.base_url = "http://127.0.0.1:8000/register"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_user_can_register_account(self):
        driver = self.driver
        driver.get('http://127.0.0.1:8000/register')
        driver.find_element_by_id("id_username").send_keys("sampleuser11")
        driver.find_element_by_id("id_email").send_keys("sampleuser11@yahoo.com")
        driver.find_element_by_id("id_mobile_number").send_keys("09171234567")
        driver.find_element_by_id("id_password1").send_keys("password111")
        driver.find_element_by_id("id_password2").send_keys("password111")
        driver.find_element_by_css_selector("input[type=\"submit\"]").click()
    
    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True
    
    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)
"""
"""
class LogInTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.base_url = "http://127.0.0.1:8000/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def tearDown(self):
        self.drive.quit()

    def test_user_can_log_in(self):
        driver = self.driver
        self.driver.get('http://127.0.0.1:8000/login')
        driver.find_element_by_id("id_username").send_keys("sampleuser11")
        driver.find_element_by_id("id_password").send_keys("password111")
        driver.find_element_by_css_selector("input[type=\"submit\"]").click()

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True
    
    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)
"""
"""
class Register(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        #self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_register(self):

        self.browser.get('http://127.0.0.1:8000/register')

        self.assertIn('User Registration', self.browser.title)

        # Check if all fields are available
        username_box = self.browser.find_element_by_name('username')
        email_box = self.browser.find_element_by_name('email')
        mobile_number_box = self.browser.find_element_by_name('mobile_number')
        password1_box = self.browser.find_element_by_name('password1')
        password2_box = self.browser.find_element_by_name('password2')
        submit_btn = self.browser.find_element_by_name('submit_btn')

        username_box.send_keys('test_user')
        email_box.send_keys('test_user@test.com')
        mobile_number_box.send_keys('09171234567')
        password1_box.send_keys('password')
        password2_box.send_keys('password')

        submit_btn.click()

class Login(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        #self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_login(self):

        self.browser.get('http://127.0.0.1:8000/login')

        self.assertIn('Login', self.browser.title)

        username_box = self.browser.find_element_by_name('username')
        password_box = self.browser.find_element_by_name('password')
        submit_btn = self.browser.find_element_by_name('submit_btn')

        username_box.send_keys('nikka')
        password_box.send_keys('password')
        submit_btn.click()

        self.browser.implicitly_wait(3)

        # Check if logged in
        logged_in_username_box = self.browser.find_element_by_name('username')
        logout_link = self.browser.find_element_by_name('logout_link')
"""
"""
class AddAdvertisementTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        #self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_add_an_advertisement(self):
        # Jeby logs in to Dealiman and wants to add a new advertisement

        #TODO: Add code to login here

        Login.test_can_login(self)

        self.browser.get('http://127.0.0.1:8000/advertisement/add/')

        # He notices the page title and header mention add advertisement
        self.assertIn('Add Advertisement', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Add Advertisement', header_text)

        # He is invited to enter a new advertisement right away
        ad_name_box = self.browser.find_element_by_id('id_new_advertisement_name')
        self.assertEqual(
                ad_name_box.get_attribute('placeholder'),
                'Advertisement Name'
        )

        ad_description_box = self.browser.find_element_by_id('id_new_advertisement_description')
        self.assertEqual(
                ad_description_box.get_attribute('placeholder'),
                'Description'
        )

        ad_category_box = Select(self.browser.find_element_by_id('id_new_advertisement_category'))
        valid_categories = ['Books', 'Mobile', 'Computers', 'Clothes', 'Appliances', 'Furniture', 'Services']
        valid_categories_value = [x.upper() for x in valid_categories]
        self.assertEqual(len(valid_categories), len(ad_category_box.options))
        for option in ad_category_box.options:
            self.assertIn(option.text, valid_categories)
            self.assertIn(option.get_attribute('value'), valid_categories_value)
        self.assertEqual('Books', ad_category_box.first_selected_option.text)

        ad_price_box = self.browser.find_element_by_id('id_new_advertisement_price')
        self.assertEqual(ad_price_box.get_attribute('placeholder'), 'Price')

        ad_price_negotiable_box = self.browser.find_element_by_id('id_new_advertisement_price_negotiable')
        self.assertFalse(ad_price_negotiable_box.is_selected())

        ad_is_used_box = self.browser.find_element_by_id('id_new_advertisement_is_used')
        self.assertFalse(ad_is_used_box.is_selected())

        ad_quantity_box = self.browser.find_element_by_id('id_new_advertisement_quantity')
        self.assertEqual(ad_quantity_box.get_attribute('placeholder'), 'Quantity')

        ad_pic_1_box = self.browser.find_element_by_id('id_new_advertisement_pic_1')
        ad_pic_2_box = self.browser.find_element_by_id('id_new_advertisement_pic_2')
        ad_pic_3_box = self.browser.find_element_by_id('id_new_advertisement_pic_3')
        ad_pic_4_box = self.browser.find_element_by_id('id_new_advertisement_pic_4')
        ad_pic_5_box = self.browser.find_element_by_id('id_new_advertisement_pic_5')

        ad_submit_btn = self.browser.find_element_by_id('advertisement_submit_btn')
        self.assertEqual(ad_submit_btn.get_attribute('value'), 'Submit')

        # Type in new advertisement
        # Type in New Ad Name
        ad_name_box.send_keys('TC7')
        # Type in Description
        ad_description_box.send_keys('Used TC7 by Leithold')
        # Type in Category
        ad_category_box.select_by_value('BOOKS')
        # Type in Price
        ad_price_box.send_keys('600.00')

        ad_price_negotiable_box.click()
        ad_is_used_box.click()
        ad_quantity_box.send_keys('1')

        ad_pic_1_box.send_keys(os.getcwd()+"upload_files/TC7_1.jpg")
        ad_pic_2_box.send_keys(os.getcwd()+"upload_files/TC7_2.jpg")
        ad_pic_3_box.send_keys(os.getcwd()+"upload_files/TC7_3.jpg")

        ad_submit_btn.click()

        # check if valid html returned for success
        self.assertIn('Add Advertisement', self.browser.title)
        success_header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Successfully Added New Advertisement', success_header_text)


class ApproveAdvertisementTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        #self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_approve_advertisement(self):
        # Admin logs in to Dealiman to view advertisements for approval
        # TODO: Add code to login here
        self.browser.get('http://127.0.0.1:8000/administration/for_approval/')


        # He notices the page title and header mention advertisements for approval
        self.assertIn('Advertisements for Approval', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Advertisements for Approval', header_text)

        # He sees a list of advertisements for approval
        advertisement_table = self.browser.find_element_by_id('approval_list_table')
        
        # He sees the ads that are members of this list
        advertisement_rows = advertisement_table.find_elements_by_tag_name("tr")
        for ad_row in advertisement_rows:
            cell = ad_row.find_elements_by_tag_name("td")
            
            links = list()
            for i in cell:
                # Each entry contains an anchor link to manage each ad
                link = i.find_elements_by_tag_name("a")[0].get_attribute("href")
                
                # The link should look like http://domain/administration/for_approval/show/AD_ID
                self.assertRegexpMatches(link,r"^http://.*/administration/for_approval/show/[0-9]{1,8}$")

                # Take note of the links for processing
                links.append(link)
                
        # He processes the ads for approval
        for link in links:
            
            # He clicks the link of an ad
            self.browser.get(link)

            # He notices the page title and header mention the advertisement for approval
            self.assertIn('Approve an Advertisement', self.browser.title)
            header_text = self.browser.find_element_by_tag_name('h1').text
            self.assertIn('Approve an Advertisement', header_text)

            # He notices the ad title in the header (1-50 characters)
            #ad_header = self.browser.find_element_by_id('item_name_header').text
            #self.assertRegexpMatches(ad_header,r"^.{1,50}$")

            # He notices the post date
            #ad_post_date = self.browser.find_element_by_id('item_post_date').text
            #self.verifyTextPresent(ad_post_date)

            # He notices quantity
            #ad_post_date = self.browser.find_element_by_id('item_post_date').text
            #self.verifyTextPresent(ad_post_date)


            
        self.fail('Finish the test!')

"""

class UserCanAddAdvertisements(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_user_can_add_advertisements(self):
        driver = self.driver
        driver.get(self.base_url + "/advertisement/add/")
        driver.find_element_by_id("id_new_advertisement_name").send_keys("Oliver Twist")
        driver.find_element_by_id("id_new_advertisement_description").send_keys("by Charles Dickens")
        Select(driver.find_element_by_id("id_new_advertisement_category")).select_by_visible_text("Books")
        driver.find_element_by_id("id_new_advertisement_price").send_keys("150")
        # ERROR: Caught exception [ERROR: Unsupported command [isEditable | id=id_new_advertisement_price_negotiable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [isEditable | id=id_new_advertisement_is_used | ]]
        driver.find_element_by_id("id_new_advertisement_quantity").send_keys("1")
        # ERROR: Caught exception [ERROR: Unsupported command [isEditable | id=id_new_advertisement_pic_1 | ]]
        driver.find_element_by_id("advertisement_submit_btn").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

class Pw1And2DoesNotMatch(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_pw1_and2_does_not_match(self):
        driver = self.driver
        driver.get(self.base_url + "/register/")
        driver.find_element_by_id("id_username").send_keys("username_reg")
        driver.find_element_by_id("id_email").send_keys("email@reg.com")
        driver.find_element_by_id("id_mobile_number").send_keys("09090909090")
        driver.find_element_by_id("id_password1").send_keys("word1")
        driver.find_element_by_id("id_password2").send_keys("word2")
        driver.find_element_by_name("submit_btn").click()
        self.assertEqual("\"The two password fields did not match.\"", driver.find_element_by_css_selector("ul.errorlist.nonfield > li").text)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

class UsernameAndPasswordDoesNotMatch(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_username_and_password_does_not_match(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("id_username").send_keys("pamferrer")
        driver.find_element_by_id("id_password").send_keys("wrongpass")
        driver.find_element_by_css_selector("input[type=\"submit\"]").click()
        self.assertEqual("Django Login Application | Login Login Username: Password: Register", driver.find_element_by_css_selector("html").text)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

class ApproveAd(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_approve_ad(self):
        driver = self.driver
        driver.get(self.base_url + "/administration/for_approval/show/1")
        driver.find_element_by_name("action").click()
        self.assertEqual("Approve an Advertisement", driver.title)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

class RejectAnAd(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_reject_an_ad(self):
        driver = self.driver
        driver.get(self.base_url + "/administration/for_approval/show/2")
        driver.find_element_by_xpath("(//input[@name='action'])[2]").click()
        self.assertEqual("Approve an Advertisement", driver.title)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == '__main__':
    unittest.main(warnings='ignore')