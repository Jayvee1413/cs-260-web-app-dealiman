from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.test import TestCase

from dealiman.forms import RegistrationForm
from dealiman.views import register


# Create your tests here.

class RegisterPageTest(TestCase):

    def test_register_url_resolves_to_register_view(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    # this has issue in testing due to the csrf token
    '''
    def test_register_returns_correct_html(self):
        request = HttpRequest()
        response = register(request)
        form = RegistrationForm()
        variables = RequestContext(request, {
    'form': form
    })
        expected_html = render_to_string('registration/register.html', variables)
        print(expected_html)
        self.assertEqual(response.content.decode(), expected_html)
    '''

    def test_register_user(self):
        request = HttpRequest()
        post_data = {
            'username': 'test_user',
            'email': 'test_user@gmail.com',
            'mobile_number': '09171234567',
            'password1': 'password',
            'password2': 'password'
        }

    def test_valid_data(self):
        form = RegistrationForm({
            'username': 'test_user',
            'email': 'test_user@gmail.com',
            'mobile_number': '09171234567',
            'password1': 'password',
            'password2': 'password'
        })
        self.assertTrue(form.is_valid())
        app_user = form.save()

"""
class AddAdvertisementPageTest(TestCase):

    def test_add_advertisement_url_resolves_to_add_advertisement_view(self):
        found = resolve('/advertisement/add/')
        self.assertEqual(found.func, add_advertisement)

    def test_add_advertisement_returns_correct_html(self):
        request = HttpRequest()
        response = add_advertisement(request)
        form = AddAdvertisementForm()
        expected_html = render_to_string('advertisement/add_advertisement.html', {'form': form})
        self.assertEqual(response.content.decode(), expected_html)


class ApproveAdvertisementPageTest(TestCase):

    #Test if URL resolves properly
    def test_approve_advertisement_url_resolves_to_approve_advertisement_view(self):
        found = resolve('/administration/for_approval/')
        self.assertEqual(found.func, admin_list_ads_for_approval)

    #Test if advertisements are listed on the page
    def test_list_approve_advertisement_returns_correct_html(self):
        request = HttpRequest()
        response = admin_list_ads_for_approval(request)
        for_approval = [
                        {"ad_id": 10001 , "item_name" : "iPhone 6 64GB Space Gray"},
                        {"ad_id":10002 ,  "item_name" : "iPhone 6 64GB Gold"},
                        ]
        expected_html = render_to_string('administration/list_for_approval.html', {'for_approval': for_approval})
        self.assertEqual(response.content.decode(), expected_html)

    #Test if page renders correct message if there are no ads for for_approval
    #def test_approve_advertisement_returns_correct_html_no_ads(self):
    #    request = HttpRequest()
    #    response = admin_list_ads_for_approval(request)
    #    for_approval = None
    #    expected_html = render_to_string('administration/list_for_approval.html', {'for_approval': for_approval})
    #    self.assertEqual(response.content.decode(), expected_html)

    #Test if advertisement for approval is properly displayed
    def test_approve_advertisement_returns_correct_html(self):
        request = HttpRequest()
        response = admin_show_ad_for_approval(request)
        ad_data = {
                "ad_id" : 10001,
                "user" : "topher",
                "contact" : "+639171234567",
                "item_name" : "iPhone 6 64GB Space Gray",
                "item_description" : "Memory:64 GB\nColor:Space Gray\nGlobe-locked\nItem in good condition",
                "item_category" : "Mobile",
                "item_price" : "{:,.2f}".format(30000),
                "item_price_negotiable" : True,
                "item_quantity" : 1,
                "item_is_used" : True,
                "posting_date" : datetime.datetime.strptime("2015-11-24 11:11:11",'%Y-%m-%d %H:%M:%S'),
                "photos" : ["iphone1.jpg","iphone2.jpg","iphone3.png","iphone4.jpg"],
            }
        expected_html = render_to_string('administration/show_for_approval.html', {'ad': ad_data})
        self.assertEqual(response.content.decode('utf-8'), expected_html)


"""